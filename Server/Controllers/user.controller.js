const userModel = require('../Models/user');
const userCtrl = {};

userCtrl.getusers = async(req, res)=>{
    //este es un proceso lento, por eso espera (await) y cuando lo tengas, me lo guardas en la constante.
   //find es una manera de decir que busque todos los usuarios de la base de datos
    const allUsers = await userModel.find()
    //cuando tengas los datos, mandalos como RESpuesta al navegador en formato json
    res.json(allUsers);
};
//CREA UN USUARIO
userCtrl.createuser = async (req, res) =>{
    try{
    console.log("este es el body que se envía ", req.body);
    const user = new userModel(req.body);
    await user.save();
    console.log(user);
    res.json("Usuario creado con éxito")
}catch(err){
    console.log(err);
}
} 
//OBTIENE UN USUARIO EN ESPECÍFICO 
userCtrl.getUserDetail = async (req, res) =>{
    console.log(req.params.id);
    const user = await userModel.findById(req.params.id);
    res.json(user);
}
//EDITA O MODIFICA UN USUARIO
userCtrl.editUser = async (req, res) =>{
    const usuario = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    }
    //busca un usuario por el id que te voy a pasar en un argumento, y cambia los valores por los que te seteo
    //new: true significa, si no existe, créalo
    await userModel.findByIdAndUpdate(req.params.id, {$set:usuario}, {new: true});
    res.json({status: 'Usuario actualizado con éxito'});
}
//ELIMINA UN USUARIO 
userCtrl.deleteUser = async (req, res) =>{  
 await userModel.findByIdAndRemove(req.params.id);
   res.json({status: "Empleado Eliminado con éxito"});
}
module.exports = userCtrl;