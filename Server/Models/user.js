const  mongoose = require('mongoose');
//de mongoose quiero únicamente los esquemas 
const{ Schema } = mongoose;


//modelo de datos para la base de datos, como se van a ver los datos
const usersSchema = new Schema({
    username: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true}
})
//lo exportamos para usarlo en otros archivos de nuestro proeycto
module.exports = mongoose.model('user', usersSchema );

