//express hace referencia al framework de Nodej.js Express
const express = require('express');
//hacer referencia al modulo de node llamado Morgan
const morgan = require('morgan');

const cors = require('cors');
//app es un objeto que contiene toda la funcionalidad de mi servidor 
const app = express();
//requiero solamente mongoose, que de alguna manera solo devuelve la conexión
const { mongoose } = require('./database');
const  bodyParser = require('body-parser')
// support parsing of application/json type post data
app.use(bodyParser.json());
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));
//Settings 
//Configuracion el puerto donde se ejecuta nuestra app
//set es una variable para acceder a ella desde cualquier parte de nuestra app la variable es port = 3000
//proces.env.port = si existe un puerto dado por la nube o por el SO tomalo, sino, el puerto es 3000
app.set('port', process.env.PORT || 3000)

//Middlewares (funciones que procesan datos)
//Morgan te dará una ayuda, como por ejemplo mostrarte errores 404, si tu peticion es GET y otros
app.use(morgan('dev'));
//le digo a cors que se puede comunicar con un servidor que hay en 4200 para que no bloqueé conexiones

//acuerdate que ahora hay que poner esta ruta para desplegar front
app.use(cors({origin: 'http://127.0.0.1:4200'}));


app.use(function(req, res, next) {
    req.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.get('/no-content', function(req, res){
  res.status(204).send();
});

//esto nos ayuda a entender los objetos JSON que vienen desde el navegador
app.use(express.json());
app.get('/no-content', function(req, res){
  res.status(204).send();
});

//Routes (se encarga de iniciar el servidor)
//añade le prefijo api/users
app.use('/api/users',require('./Routes/users.routes'));
//Definimos en que puerto escucha nuestro servidor, port que es = 3000
app.listen(app.get('port'), ()=>{
    console.log("Server on port", app.get('port'));
});