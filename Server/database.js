//modulo mongoose
const mongoose = require('mongoose');
//la direccion de nuestra base de datos 
const URI = 'mongodb://localhost/mean-crud';

//conecta con nuestra base de datos, devuelve la conexión mas bien.
mongoose.connect(URI)
        .then(db => console.log("db is connected"))
        .catch(err=> console.error(err));
        
module.exports.mongoose;