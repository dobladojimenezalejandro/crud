//obtenemos el modulo express
const express = require('express');
//esta es la ruta del contorlador del user
const userCtrl = require('../Controllers/user.controller');
//router guardará las rutas que luego exportaremos.
const router = express.Router();
//cuando pidan una petición get a la ruta inicial del servidor envuio una request que diga "hello world"
router.get('/', userCtrl.getusers);
//ruta hacia el servicio que crea un usuario
router.post('/', userCtrl.createuser);
//ruta hacia el servicio que obtiene el detealle de un solo usuario
router.get('/:id', userCtrl.getUserDetail);
//actualizar datos del usuario
router.put('/:id', userCtrl.editUser);
//eliminar usuario
router.delete('/:id', userCtrl.deleteUser);
module.exports = router;