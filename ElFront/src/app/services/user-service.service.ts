import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  public selectedUser: User;
userMockeado = {
  "_id": "2123111132323",
	"username": "Tany",
    "email": "tany@yahoo.com",
    "password": "tanytany"
}
  readonly URL_API = 'http://localhost:3000/api/users';
  users: User[];

  constructor(private http: HttpClient) {
  this.selectedUser = new User();
  }

// OBTENER TODOS LOS USUARIOS
  getUsers() {
    return this.http.get(this.URL_API);
  }
  // AÑADIR NUEVO USUARIO
  postUser(user: User) {
    return this.http.post(this.URL_API, user);
  }
  // ACTUALZIAR USUARIO
// primer argumento la url, que necesita /idUser y segundo argumento, los datos actualizados
  putUser(user: User){
    return this.http.put(this.URL_API + `/${user._id}`, user);
  }
  // ELIMINAR EMPLEADO por id
  deleteUser(_id: string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }

}
