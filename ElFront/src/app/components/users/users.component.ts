import { User } from './../../models/user';
import { UserServiceService } from './../../services/user-service.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserServiceService]
})
export class UsersComponent implements OnInit {
  constructor(public UserService: UserServiceService) {}

  ngOnInit(): void {}

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.UserService.selectedUser = new User();
    }
  }

  addUser(form: NgForm) {
    this.UserService.postUser(form.value).subscribe(res => {
     console.log(res);

   }, err => {
     console.log(err);
   });
  }
}
